%global ly_version 2.25.12

Name:           lilypond-gonville
Version:        %{ly_version}
Release:        %{autorelease}
Summary:        An alternative lilypond command that uses the Gonville fonts

# This package is a very short script; there is no copyrightable content.
License:        Public Domain
URL:            http://www.chiark.greenend.org.uk/~sgtatham/gonville/
Source0:        lilypond-gonville
Source1:        gonville-default.ly

BuildArch:      noarch
Requires:       gonville-fonts >= 20231028
Requires:       gonville-fonts-svg
Requires:       lilypond >= 2.25.5

%description
Gonville is a font of symbols for typesetting music: clefs, note heads, quaver
tails, and so on. It is compatible with GNU Lilypond.

This package provides a wrapper script lilypond-gonville to run Lilypond with
Gonville as the default music font.

%build
cp -p %{SOURCE0} lilypond-gonville
cp -p %{SOURCE1} gonville-default.ly

%install
mkdir -p %{buildroot}%{_bindir}
install -p lilypond-gonville %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/%{name}
cp -p gonville-default.ly %{buildroot}%{_datadir}/%{name}

%files
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/%{name}/gonville-default.ly

%changelog
* Fri Mar 01 2024 Peter Simonyi <pts@petersimonyi.ca> - 2.25.12-1
- Switch to LP 2.25 font selection mechanisms
* Wed Nov 01 2017 Peter Simonyi <pts@petersimonyi.ca> - 2.19.80
- Update prep for f26 macro changes
- Bump Lilypond version number
* Mon May 30 2016 Peter Simonyi <pts@petersimonyi.ca> - 2.19.42
- Bump Lilypond version number
* Fri Mar 11 2016 Peter Simonyi <pts@petersimonyi.ca> - 2.19.37
- Bump Lilypond version number
* Wed Mar 2 2016 Peter Simonyi <pts@petersimonyi.ca> - 2.19.36
- Bump Lilypond version number
* Mon Jan 25 2016 Peter Simonyi <pts@petersimonyi.ca> - 2.19.35
- First package attempt

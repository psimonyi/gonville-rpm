% Include file to make Gonville the default font.
%
% This replaces the one included in the Gonville distribution, which is not
% compatible with LilyPond > 2.25.4.
%
% To use, run lilypond -d include-settings=gonville-default.ly
% or use lilypond-gonville instead.

\paper {
    property-defaults.fonts.music = "gonville"
}

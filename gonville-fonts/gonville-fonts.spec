%global gonville_version 20231028.4ec6d4f

# This spec file creates two packages:
# gonville-fonts: the OTF font files
# gonville-fonts-svg: the SVG versions of the above, for Lilypond's SVG backend
# See also lilypond-gonville for a wrapper script.

Version:        20231028
Release:        %{autorelease}
URL:            https://www.chiark.greenend.org.uk/~sgtatham/gonville/

# Note that the source is MIT; only its output is PD.
%global fontlicense Public Domain
%global fontlicenses LICENCE
%global fontdocs README
%global fontdocsex %{fontlicenses}

%global fontfamily Gonville
%global fontsummary Gonville, a font of symbols for typesetting music
%global fontpkgheader %{expand:
Suggests: %{name}-svg
}

%global fonts lilyfonts/gonville*.otf

%global fontdescription %{expand:
Gonville is a font of symbols for typesetting music: clefs, note heads, quaver
tails, and so on.  It is compatible with GNU LilyPond.  It was drawn by Simon
Tatham to replace LilyPond's then-standard font Feta.  Gonville has a plainer,
slightly more modern look.

Gonville cannot completely replace the standard LilyPond fonts.  It does not
attempt to reproduce shape note heads, ancient music notation, or the longa
note (which is also unused in modern music).
}

Source0: https://www.chiark.greenend.org.uk/~sgtatham/gonville/gonville-%{gonville_version}-src.tar.gz

# Required for glyphs.py
BuildRequires:  python3, fontforge, ghostscript-core, potrace
# Required for svgfilter
BuildRequires: make, gcc, flex

%fontpkg

%package svg
Summary: Gonville, a font of symbols for typesetting music (SVG format)
%description svg
The Gonville music font for LilyPond available in gonville-fonts contains OTF
fonts for LilyPond's PostScript and PDF backends.  This package contains the
same glyphs in SVG format for the SVG backend.


%prep
%setup -q -n gonville-%{gonville_version}

%build
# Upstream's build script 'Buildscr' is written in a custom language 'bob' and
# produces some extra files, but is a useful guide.
pushd svgfilter
make
popd
./glyphs.py --ver="%{gonville_version}" --svgfilter=./svgfilter/svgfilter --lily
mv README.post-2.20 README
%fontbuild

%install
%fontinstall
mkdir -p %{buildroot}%{_datadir}/gonville-fonts-svg
cp -p lilyfonts/gonville*.svg %{buildroot}%{_datadir}/gonville-fonts-svg/

%check
%fontcheck

%fontfiles

%files svg
%{_datadir}/gonville-fonts-svg

%changelog
* Sun Dec 17 2023 Peter Simonyi <pts@petersimonyi.ca> - 20231028-1
- Update to 20231028.4ec6d4f, which includes support for newwer versions of
  Lilypond and some new glyphs.
- Provide only the Gonville font files for Lilypond 2.20's font-switching
  mechanism (i.e. no more Gonville files named Emmentaler).
* Sun Nov 24 2019 Peter Simonyi <pts@petersimonyi.ca> - 20141025-4
- Fix Python 2 build dep
* Fri Dec 21 2018 Peter Simonyi <pts@petersimonyi.ca> - 20141025-3
- Update URLs to HTTPS, now that upstream supports it
- Fix build requirements
* Tue Jan 26 2016 Peter Simonyi <pts@petersimonyi.ca> - 20141025-1
- Initial packaging
